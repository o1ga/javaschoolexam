package com.tsystems.javaschool.tasks.pyramid;

import java.util.*;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */

    private int rowNumber = 1;

    public int[][] buildPyramid(List<Integer> inputNumbers) {
        validateInputList(inputNumbers);
        Map<Integer, int[]> pyramidMap = new HashMap<>();
        pyramidMap = createPyramidMap(pyramidMap, inputNumbers); // map where keys are rows and values numbers
        int pyramidHeight = pyramidMap.size();
        int[][] result = new int[pyramidHeight][];
        result = BuildPyramidFromMap(pyramidMap, result, pyramidHeight);
        return result;
    }

    private int[][] BuildPyramidFromMap(Map<Integer, int[]> pyramidMap, int[][] result, int pyramidHeight) {
        int resultIndex = 0;
        Iterator it = pyramidMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            int row = (int) pair.getKey();
            int[] values = (int[]) pair.getValue();
            int rowWidth = pyramidHeight * 2 - 1;
            int[] arrayWithZeros = new int[rowWidth]; // create new row with zeros
            int spaceOnCorners = pyramidHeight - row; // lower row is, less space is left on sides
            arrayWithZeros = insertValuesToPyramid(spaceOnCorners, values, rowWidth, arrayWithZeros);
            result[resultIndex] = arrayWithZeros;
            resultIndex++;
            if (resultIndex == pyramidHeight && arrayWithZeros[rowWidth - 1] == 0) {
                throw new CannotBuildPyramidException();
            }
        }
        return result;
    }

    private int[] insertValuesToPyramid(int spaceOnCorners, int[] values, int rowWidth, int[] arrayWithZeros) {
        int indexValues = 0;
        Boolean isSpaceBetweenNumbers = false; // leave zeros between values
        for (int i = 0; i < arrayWithZeros.length; i++) {
            if (values.length == indexValues) {
                isSpaceBetweenNumbers = false;
                break;
            }
            if (i >= spaceOnCorners && i <= rowWidth - spaceOnCorners) { // leave space on borders left and write
                if (!isSpaceBetweenNumbers.booleanValue()) {
                    arrayWithZeros[i] = values[indexValues];
                    indexValues++; // go to next value to insert
                    isSpaceBetweenNumbers = true;
                } else isSpaceBetweenNumbers = false;
            }
        }
        return arrayWithZeros;
    }

    private Map<Integer, int[]> createPyramidMap(Map<Integer, int[]> pyramidMap, List<Integer> inputNumbers) {
        for (int i = 0; i < inputNumbers.size(); i++) {
            Integer number = inputNumbers.get(i);
            int[] values;
            if (pyramidMap.containsKey(rowNumber)) { // if row exists get values
                values = pyramidMap.get(rowNumber);
            } else {
                values = new int[rowNumber]; // create new row, cell quantity equals number of row
            }
            updateRowWithValues(pyramidMap, number, values);
        }
        return pyramidMap;
    }

    private void validateInputList(List<Integer> inputNumbers) {
        if (inputNumbers == null) throw new CannotBuildPyramidException();
        try {
            inputNumbers.sort(Comparator.naturalOrder());
        } catch (Throwable e) {
            throw new CannotBuildPyramidException();
        }
    }

    private void updateRowWithValues(Map pyramidMap, int number, int[] values) {
        for (int j = 0; j < values.length; j++) {
            if (values[j] == 0) { // if cell is empty add number
                values[j] = number;
                pyramidMap.put(rowNumber, values);
                if (values.length - 1 == j) { // if iteration finished start next row
                    rowNumber++;
                    break;
                }
                break;
            } else if (j == values.length - 1) rowNumber++;
        }
    }
}
