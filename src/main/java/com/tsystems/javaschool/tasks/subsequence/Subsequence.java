package com.tsystems.javaschool.tasks.subsequence;

import java.util.ArrayList;
import java.util.List;

public class Subsequence {
    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        validateInput(x, y);
        if (x.size() > y.size()) return false;
        List<Integer> matchingElementsIndices = new ArrayList();
        for (int i = 0; i < x.size(); i++) {
            for (int j = 0; j < y.size(); j++) {
                if (x.get(i) == y.get(j)) {
                    matchingElementsIndices.add(j);
                    break;
                }
            }
        }
        if (matchingElementsIndices.size() == x.size() && isSorted(matchingElementsIndices)) {
            return true;
        }
        return false;
    }

    private void validateInput(List x, List y) {
        if (x == null || y == null) throw new IllegalArgumentException();
    }

    private boolean isSorted(List<Integer> list) {
        for (int i = 0; i < list.size() - 1; i++) {
            if (list.get(i) > list.get(i + 1))
                return false;
        }
        return true;
    }
}
