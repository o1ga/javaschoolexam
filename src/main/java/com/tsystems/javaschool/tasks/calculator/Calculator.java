package com.tsystems.javaschool.tasks.calculator;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     * parentheses, operations signs '+', '-', '*', '/'<br>
     * Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    private List<Character> operators = Arrays.asList('/', '*', '-', '+');
    private List<Character> numbers = Arrays.asList('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');


    public String evaluate(String statement) {
        if (statement == null || statement == "") return null;
        String answer = "";
        List<String> statementAsList = getStatementAsList(statement);
        if (statementAsList == null) return null;
        statementAsList = calculateOperationsGroups(statementAsList); // calculates groups inside parenthesis
        answer = performOperations(statementAsList);
        if (answer == null) return null;
        String roundedAnswer = roundNumber(answer);
        return roundedAnswer;
    }

    String roundNumber(String stringNumber) {
        Float roundedAnswer = Math.round(Float.parseFloat(stringNumber) * 10000f) / 10000f;
        if (roundedAnswer % 1 == 0) return String.valueOf(Math.round(roundedAnswer));
        return roundedAnswer.toString();
    }

    //This method searches for groups of operations in parenthesis and calculates them
    List<String> calculateOperationsGroups(List<String> statementAsList) {
        while (statementAsList.contains("(")) {
            int startIndex = statementAsList.indexOf(("("));
            int endIndex;
            if (statementAsList.contains(")")) {
                endIndex = statementAsList.lastIndexOf((")"));
            } else return null; // there should be open and close parenthesis
            List<String> group = statementAsList.subList(startIndex + 1, endIndex);
            group = calculateOperationsGroups(group); // in case there is a group inside another group
            performOperations(group);
            statementAsList.remove(startIndex + 2); // remove second number
            statementAsList.remove(startIndex); // remove first number
        }
        return statementAsList;
    }

    //This function searches for an operator in the list
    // and calls method to calculate until no operators are left
    String performOperations(List<String> list) {
        for (Character operator : operators) {
            while (list.contains(operator.toString())) {
                if (list.size() >= 3) {
                    int index = list.indexOf(operator.toString());
                    list = performOperation(list, index);
                    if (list == null) return null;
                } else return null;
            }
        }
        if (list.size() == 1) return list.get(0);
        else return null;
    }

    //This function does a math operation with two numbers and updates the list with result,
// removing numbers and operator
    private List<String> performOperation(List<String> list, int index) {
        String number1 = list.get(index - 1);
        String operator = list.get(index);
        String number2 = list.get(index + 1);
        String result = calculate(number1, number2, operator.charAt(0));
        if (result == null) return null;
        list.set(index, result);
        list.remove(index + 1);
        list.remove(index - 1);
        return list;
    }

    private String calculate(String number1, String number2, Character operator) {
        String answer = "";
        switch (operator) {
            case '*':
                answer = multiply(number1, number2);
                break;
            case '-':
                answer = subtract(number1, number2);
                break;
            case '+':
                answer = sum(number1, number2);
                break;
            case '/':
                answer = divide(number1, number2);
                break;
        }
        return answer;
    }

    private boolean isNumber(Character c) {
        if (c != null) return numbers.contains(c);
        else return false;
    }

    private boolean isOperator(char c) {
        return operators.contains(c);
    }

    private String multiply(String x, String y) {
            Float xNumber = Float.parseFloat(x);
            Float yNumber = Float.parseFloat(y);
            Float result = xNumber * yNumber;
            return result.toString();
    }

    private String divide(String x, String y) {
        if (y.equals("0.0") || y.equals("0")) return null;
        Float xNumber = Float.parseFloat(x);
        Float yNumber = Float.parseFloat(y);
        Float result = xNumber / yNumber;
        return result.toString();
    }

    private String sum(String x, String y) {
            Float xNumber = Float.parseFloat(x);
            Float yNumber = Float.parseFloat(y);
            Float result = xNumber + yNumber;
            return result.toString();
    }

    private String subtract(String x, String y) {
            Float xNumber = Float.parseFloat(x);
            Float yNumber = Float.parseFloat(y);
            Float result = xNumber - yNumber;
            return result.toString();
    }

    /*
 This method converts a string with math statement into a list for further processing
 or returns null if statement is not valid.
 Example: "10/(2-7+3)*4" -> {"10", "/", "(", "2", "-", "7", "+", "3", ")", "*", "4"}
*/
    List<String> getStatementAsList(String statement) {
        List<String> statementAsList = new LinkedList<>();
        Character actualChar;
        String number = null;
        Character previousChar = null;
        for (int i = 0; i < statement.length(); i++) {
            actualChar = statement.charAt(i);
            if (actualChar == '.') {
                if (isNumber(previousChar)) number = number + actualChar.toString();
                else return null;
            } else if (isNumber(actualChar)) {
                if (previousChar != null) {
                    if (isNumber(previousChar) || previousChar == '.') number = number + actualChar.toString();
                    else number = actualChar.toString();
                } else if (previousChar == null) number = actualChar.toString();
            } else if (isOperator(actualChar)) {
                if (previousChar != null && isOperator(previousChar)) return null;
                if (isNumber(previousChar)) {
                    statementAsList.add(number);
                    number = null;
                }
                statementAsList.add(actualChar.toString());
            } else if (actualChar == '(' || actualChar == ')') {
                if (isNumber(previousChar)) {
                    statementAsList.add(number);
                    number = null;
                }
                statementAsList.add(actualChar.toString());
            } else return null;
            previousChar = actualChar;
        }
        if (number != null) statementAsList.add(number);
        if (Collections.frequency(statementAsList, "(") != Collections.frequency(statementAsList, ")")) return null;
        return statementAsList;
    }
}