package com.tsystems.javaschool.tasks;

import com.tsystems.javaschool.tasks.calculator.Calculator;
import com.tsystems.javaschool.tasks.pyramid.PyramidBuilder;

import java.util.Arrays;
import java.util.List;

public class Main{
    public static void main(String[] args){
        Calculator calculator = new Calculator();
        //System.out.println(calculator.evaluate("10/2-7+3*4"));
        System.out.println(calculator.evaluate("((12+1)*3.1)"));
        //PyramidBuilder pyramidBuilder = new PyramidBuilder();
        //System.out.println(Arrays.toString(pyramidBuilder.buildPyramid(Arrays.asList(1, 3, 2, 9, 4, 5))));

    }
}

